# LinköpingsUtmaningen

LinköpingsUtmaningen is a quiz game in the form of a web application.

It is built using [Meteor](https://www.meteor.com).

## Running

Install Meteor.

Execute the following in the project directory:

```
meteor run
```

Then visit **127.0.0.1:3000/admin** and generate some questions (which will query the API).

After this, you should be able to login at **127.0.0.1:3000**.
You will need to get a friend to sign in as well, if you want to play the actual game.